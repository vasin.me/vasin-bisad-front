//
// ─── IMPORT PACKAGE BLOCK ───────────────────────────────────────────────────────
//
import Vue from "vue";
import App from "./App.vue";
import router from "./router/router";
import store from "./store/store";
import BootstrapVue from "bootstrap-vue";
import VueSweetalert2 from "vue-sweetalert2";
import VueResource from "vue-resource";
import VueLazyLoad from "vue-lazyload";
import VueCookies from "vue-cookies";

// ─── IMPORT FILE BLOCK ───────────────────────────────────────────────────────────────
//
import "./registerServiceWorker";
import "@scss/global.scss";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

//
// ─── USE BLOCK ──────────────────────────────────────────────────────────────────
//
Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(VueResource);
Vue.use(VueCookies);
Vue.use(VueLazyLoad, {
  preLoad: 1.3,
  loading: "./src/logo.png",
  attempt: 1
});

//
// ─── VUE INJECTION ──────────────────────────────────────────────────────────────
//
Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
