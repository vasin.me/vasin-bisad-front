import Vue from "vue";
import Router from "vue-router";
import isEmpty from "lodash.isempty";
import store from "@/store/store";
import { Action } from "vuex-class";
import { AuthState, UpdateState } from "../store/auth/types";

const namespace: string = "auth";

//
// ─── IMPORT COMPONENT ──────────────────────────────────────────────────────────────────
//

const Header = () => import("@components/Header.vue");
const Body = () => import("@components/Body.vue");

//
// ─── IMPORT VIEWS ───────────────────────────────────────────────────────────────
//

const Auth = () => import("@views/Auth/Auth.vue");
const Dashboard = () => import("@views/Dashboard/Dashboard.vue");
const _Main = () => import("@views/Dashboard/_Main.vue");
const _Track = () => import("@views/Dashboard/_Track.vue");
const _Realtime = () => import("@views/Dashboard/_Realtime.vue");
const _Summary = () => import("@views/Dashboard/_Summary.vue");
const _Find = () => import("@views/Dashboard/_Find.vue");
const _Insight = () => import("@views/Dashboard/_Insight.vue");
const _Feeling = () => import("@views/Dashboard/_Feeling.vue");
const _Talking = () => import("@views/Dashboard/_Talking.vue");
const _Tinking = () => import("@views/Dashboard/_Tinking.vue");
const _Number = () => import("@views/Dashboard/_Number.vue");
const _ReplyRate = () => import("@views/Dashboard/_ReplyRate.vue");

//
// ─── ROUTER ─────────────────────────────────────────────────────────────────────
//

Vue.use(Router);
const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      components: {
        header: Header,
        body: Body
      },
      children: [
        {
          path: "login",
          alias: "/",
          component: Auth,
          name: "Login",
          meta: {
            login: 0
          }
        },
        {
          path: "dashboard",
          component: Dashboard,
          name: "Dashboard",
          meta: {
            login: 1
          },
          children: [
            {
              path: "trend",
              alias: "/",
              component: _Main,
              name: "Dashboard_Main",
              meta: {
                login: 1
              }
            },
            {
              path: "track",
              component: _Track,
              name: "Dashboard_Track",
              meta: {
                login: 1
              }
            },
            {
              path: "realtime",
              component: _Realtime,
              name: "Dashboard_Realtime",
              meta: {
                login: 1
              }
            },
            {
              path: "summary",
              component: _Summary,
              name: "Dashboard_Summary",
              meta: {
                login: 1
              }
            },
            {
              path: "find",
              component: _Find,
              name: "Dashboard_Find",
              meta: {
                login: 1
              }
            },
            {
              path: "insight",
              component: _Insight,
              name: "Dashboard_Insight",
              meta: {
                login: 1
              }
            },
            {
              path: "feeling",
              component: _Feeling,
              name: "Dashboard_Feeling",
              meta: {
                login: 1
              }
            },
            {
              path: "talking",
              component: _Talking,
              name: "Dashboard_Talking",
              meta: {
                login: 1
              }
            },
            {
              path: "tinking",
              component: _Tinking,
              name: "Dashboard_Tinking",
              meta: {
                login: 1
              }
            },
            {
              path: "number",
              component: _Number,
              name: "Dashboard_Number",
              meta: {
                login: 1
              }
            },
            {
              path: "replyRate",
              component: _ReplyRate,
              name: "Dashboard_ReplyRate",
              meta: {
                login: 1
              }
            }
          ]
        }
      ]
    }
  ]
});

//
// ─── MIDDLEWARE ─────────────────────────────────────────────────────────────────
//

router.beforeEach(async (to, from, next) => {
  // get user state
  // store.dispatch("fetchUser");

  // match router meta
  if (
    to.matched.some(record => record.meta.login == 0 || record.meta.login == 1)
  ) {
    // get user from cookie
    const user = Vue.cookies.get("user");

    if (to.meta.login == 0 && !isEmpty(user)) {
      // user must not signin
      return next({ name: "Dashboard_Main" });
    } else if (to.meta.login == 1 && isEmpty(user)) {
      // user must signin
      return next({ name: "Login" });
    }
  }

  next();
});

//
// ─── EXPORT ─────────────────────────────────────────────────────────────────────
//
export default router;
