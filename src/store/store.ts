import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import { RootState } from "./types";
import { auth } from "./auth/index";
import { dashboard } from "./dashboard/index";

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state: {
    version: "1.0.0"
  },
  modules: {
    auth,
    dashboard
  },
  strict: true
};

export default new Vuex.Store<RootState>(store);
