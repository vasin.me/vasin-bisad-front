export interface RootState {
  version: string;
}

export interface UpdateState {
  target: string;
  value: any;
}
