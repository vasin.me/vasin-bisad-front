import { GetterTree } from "vuex";
import { DashboardState } from "./types";
import { RootState } from "../types";

export const getters: GetterTree<DashboardState, RootState> = {
  findDataGet(state): boolean {
    return state.findData;
  },
  trendDataGet(state) {
    return state.trendData;
  },
  trackingNameGet(state) {
    return state.trackingName;
  },
  trackingNameOption(state) {
    return state.trackingNameOption;
  },
  trackingFeedGet(state) {
    return state.trackingFeed;
  },
  realtimeGet(state) {
    return state.realtime;
  },
  summaryGet(state) {
    return state.summary;
  }
};
