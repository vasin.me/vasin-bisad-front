export interface StringTMap<T> {
  [key: string]: T;
}

export interface DashboardState extends StringTMap<any> {}
