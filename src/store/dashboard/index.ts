import { Module } from "vuex";
import { actions } from "./actions";
import { RootState } from "../types";
import { DashboardState } from "./types";
import { getters } from "./getters";
import { mutations } from "./mutations";

export const state: DashboardState = {
  trendData: {},
  trackingFeed: {},
  trackingName: {},
  trackingNameOption: {},
  realtime: {},
  summary: {}
};

const namespaced: boolean = true;

export const dashboard: Module<DashboardState, RootState> = {
  namespaced,
  state,
  getters,
  mutations,
  actions
};
