import { MutationTree } from "vuex";
import { DashboardState } from "./types";
import { UpdateState } from "@/store/types";

export const mutations: MutationTree<DashboardState> = {
  updateState(state, payload: UpdateState) {
    state[payload.target] = payload.value;
  }
};
