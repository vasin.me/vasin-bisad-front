import Vue from "vue";
import isEmpty from "lodash.isempty";
import { ActionTree } from "vuex";
import { DashboardState } from "./types";
import { RootState } from "../types";

export const actions: ActionTree<DashboardState, RootState> = {
  // ! Insert Tracking
  insertTracking({ commit }, payload) {
    /**
     * - insert tracking tag
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - track_name: string, role: string
     */

    return new Promise(async (resolve, reject) => {
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/insertTrackingName`,
          payload
        );
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Find block here

  // ! Remove Tracking
  removeTracking({ commit }, payload) {
    /**
     * - remove tracking tag
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - track_name: string, role: string
     */

    return new Promise(async (resolve, reject) => {
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/removeTrackingName`,
          payload
        );
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Find block here

  // ! Get TrackingName
  queryTrackingName({ commit }, payload) {
    /**
     * - query data have word
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - keyword: list, string
     */

    return new Promise(async (resolve, reject) => {
      // ? call
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/getTrackingName`,
          payload || null
        );

        const option = await call.data.result.map(resp => {
          return resp.track_name;
        });

        await commit("updateState", {
          target: "trackingNameOption",
          value: option || []
        });

        await commit("updateState", {
          target: "trackingName",
          value: call.data.result || []
        });
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Find block here

  // ! Get TrackingFeed
  queryTrackingFeed({ commit }, payload) {
    /**
     * - query data have word
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - role: string
     */

    return new Promise(async (resolve, reject) => {
      // ? validate
      if (
        !payload ||
        !payload.brand ||
        isEmpty(payload) ||
        isEmpty(payload.brand)
      )
        return reject("bad payload.");

      // ? call
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/getTrackingFeed`,
          payload || null
        );

        commit("updateState", {
          target: "trackingFeed",
          value: call.data.result || []
        });
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Find block here

  // ! Get Realtime
  queryRealtime({ commit }, payload) {
    /**
     * - query data have word
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - keyword: list, string
     */

    return new Promise(async (resolve, reject) => {
      // ? call
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/getRealtime`,
          payload || null
        );

        console.log(call);

        commit("updateState", {
          target: "realtime",
          value: call.data.result || []
        });
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Find block here

  // ! Get Summary
  querySummary({ commit }, payload) {
    /**
     * - query data have word
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - brand: list, string
     */

    return new Promise(async (resolve, reject) => {
      // ? call
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/getSummary`,
          payload || null
        );

        commit("updateState", {
          target: "summary",
          value: call.data.result || []
        });
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Find block here

  // ! Get Summary
  saveSummary({ commit }, payload) {
    /**
     * - save summary this day
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - brand: string
     */

    return new Promise(async (resolve, reject) => {
      // ? call
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/summaryAll`,
          payload || null
        );
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Find block here

  // ! Trend
  queryTrend({ commit }, payload) {
    /**
     * - Get trend now
     *
     * @param param0 commit from mutation
     * @param payload OBJECT - filter: list, string
     */

    return new Promise(async (resolve, reject) => {
      // ? call
      try {
        const call = await (Vue as any).http.get(
          `${process.env.BACKEND_URI}/getTrend`
        );
        commit("updateState", {
          target: "trendData",
          value: call.data.result || []
        });
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Trend block here

  // ! Track
  queryTrack({ commit }, payload = { filter: "" }) {
    /**
     * - query data in tracking list
     *
     * @param param0 commit from nutation
     * @param payload OBJECT - {
     *  string('customer', 'marketing', 'executive')
     * }
     */

    return new Promise(async (resolve, reject) => {
      // ? check payload
      if (
        isEmpty(payload.filter) ||
        !["customer", "marketing", "executive"].includes(payload.filter)
      )
        return reject("bad payload data.");

      // ? call
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/track/`,
          payload
        );
        commit("updateState", {
          target: "trackData",
          value: call.data.result || []
        });
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  },
  //// End Trend block here

  // ! Campaign
  queryCampaign({ commit }, payload) {
    /**
     * - query data in tracking list
     *
     * @param param0 commit from nutation
     * @param payload OBJECT - {
     *  string('customer', 'marketing', 'executive')
     * }
     */

    return new Promise(async (resolve, reject) => {
      // ? check payload
      if (
        isEmpty(payload.filter) ||
        !["customer", "marketing", "executive"].includes(payload.filter)
      )
        return reject("bad payload data.");

      // ? call
      try {
        const call = await (Vue as any).http.post(
          `${process.env.BACKEND_URI}/campaign/`,
          payload
        );
        commit("updateState", {
          target: "campaignData",
          value: call.data.result || []
        });
        return resolve(call);
      } catch (err) {
        return reject(err);
      }
    });
  }
  //// End Trend block here
};
//// End Export block here
