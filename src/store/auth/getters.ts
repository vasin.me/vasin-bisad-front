import { GetterTree } from "vuex";
import { AuthState } from "./types";
import { RootState } from "../types";

export const getters: GetterTree<AuthState, RootState> = {
  userStateData(state): boolean {
    return state.userState;
  },
  userDataData(state): any {
    return state.userData;
  }
};
