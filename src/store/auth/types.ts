export interface StringTMap<T> {
  [key: string]: T;
}

export interface UpdateState {
  target: string;
  value: any;
}

export interface AuthState extends StringTMap<any> {
  userState: boolean;
  userData: any;
}

export interface SignInType {
  username: string;
  password: string;
}
