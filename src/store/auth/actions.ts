import Vue from "vue";
import router from "@/router/router";
import isEmpty from "lodash.isempty";
import { ActionTree } from "vuex";
import { AuthState, SignInType } from "./types";
import { UpdateState } from "@/store/types";
import { RootState } from "../types";

export const actions: ActionTree<AuthState, RootState> = {
  // ! Fetch user data
  /**
   * - get user data from cookie
   *
   * @param param0 commit from actions
   */
  fetchUser({ commit }): void {
    const updateState: UpdateState = {
      target: "userState",
      value: Vue.cookies.get("user") ? true : false
    };
    const updateData: UpdateState = {
      target: "userData",
      value: Vue.cookies.get("user")
    };

    commit("updateState", updateState || null);
    commit("updateState", updateData || null);
  },

  // ! SignIn
  /**
   * - login with username and password
   * - set username(type of user) to cookie
   * - router push to dashboard
   *
   * @param param0 commit from mutations
   * @param payload { username, password}
   */
  signin({ commit }, payload: SignInType) {
    return new Promise((resolve, reject) => {
      // validate
      if (isEmpty(payload.username) || isEmpty(payload.password))
        return reject("bad params.");
      if (!["marketing", "customer", "executive"].includes(payload.username))
        return reject("user not found.");

      // update cookie
      Vue.cookies.set("user", payload.username);

      // update state
      commit("updateState", {
        target: "userState",
        value: payload.username ? true : false
      });
      commit("updateState", {
        target: "userData",
        value: payload.username
      });

      if (payload.username == "marketing") {
        router.push({ name: "Dashboard_Find" });
      } else if (payload.username == "customer") {
        router.push({ name: "Dashboard_Talking" });
      } else {
        router.push({ name: "Dashboard_Tinking" });
      }

      // resolve
      return resolve(payload.username);
    });
  },
  // // End SignIn Block --

  // ! SignOut
  /**
   * - remove user data in cookie
   * - update auth state
   * - router push to login
   *
   * @param param0 commit from mutations
   */
  signout({ commit }): void {
    Vue.cookies.remove("user");
    router.push({ name: "Login" });

    const updateState: UpdateState = {
      target: "userState",
      value: false
    };
    const updateData: UpdateState = {
      target: "userData",
      value: ""
    };

    commit("updateState", updateState);
    commit("updateState", updateData);
  }
  // // End SignOut Block --
};
