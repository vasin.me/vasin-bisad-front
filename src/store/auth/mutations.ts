import { MutationTree } from "vuex";
import { AuthState } from "./types";
import { UpdateState } from "@/store/types";

export const mutations: MutationTree<AuthState> = {
  updateState(state, payload: UpdateState) {
    state[payload.target] = payload.value;
  }
};
