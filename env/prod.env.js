"use strict";
module.exports = {
  NODE_ENV: '"Production"',
  BACKEND_URI: JSON.stringify(process.env.BACKEND_URI)
};