// Import
const path = require("path");
const webpack = require("webpack");

module.exports = {
  chainWebpack: config => {
    // Alias config
    config.resolve.alias
      .set("@", path.resolve(__dirname, "src"))
      .set("@components", path.resolve(__dirname, "src/components"))
      .set("@views", path.resolve(__dirname, "src/views"))
      .set("@assets", path.resolve(__dirname, "src/assets"))
      .set("@scss", path.resolve(__dirname, "src/assets/scss"))
      .set("@store", path.resolve(__dirname, "src/store"))
    // Disable Eslint
    config.module.rules.delete("eslint");
  },
  configureWebpack: {
    plugins: [
      // Init Environment
      new webpack.DefinePlugin({
        "process.env": require("./env/env")
      })
    ]
  },
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "scss",
      patterns: [path.resolve(__dirname, "src/assets/scss/global.scss")]
    }
  }
};