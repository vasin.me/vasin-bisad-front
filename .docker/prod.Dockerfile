# Frontend build
FROM node:8-alpine as static

WORKDIR /home/app/bisad.vasin.me
COPY . ./

# Varaible
ARG BACKEND_URI
# ARG OMISE_CLIENT

RUN apk update && apk add git
RUN yarn install
RUN yarn build

# Nginx
FROM nginx:stable-alpine

COPY --from=static /home/app/bisad.vasin.me/dist /usr/share/nginx/html/
COPY .nginx /etc/nginx

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]